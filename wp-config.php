<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'saya' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+b0(@8ue`q$P<oLZ+IE>WLqKYNpue`UrSWeTLSwh}n4lWikn-(_m5oMpY.TFD-Mw' );
define( 'SECURE_AUTH_KEY',  ' ,eA#D)?a6g4XHS5=2<&Y&:dS.8,lo(R[CE.wxpen%3%Rc|#yWIrJhr`F|3DY(sx' );
define( 'LOGGED_IN_KEY',    'xA8xoul&]*K]~x?ZH+|:f,KIOo*a)mp)z#Y+:sA.jIj:!_74Jo`BT5Ivw9t)dpav' );
define( 'NONCE_KEY',        '#FLS`g$u+}4~}~wcV` m]f>7E<&TozaoV}NCX{(E*xNeTNf3q4_78f-Y-toU`, S' );
define( 'AUTH_SALT',        'H/pnW7=jKw{TlI/n#+9HJ]/O5mcnU|VN;*<EK<ng#iwwB%@[=^V28QDa5}N}J/2y' );
define( 'SECURE_AUTH_SALT', 'm`vY8gBWX3<yG`)ydhE3@|b~4jDW ;!QoU}x]EF^1wqR;VQfe}9 --bX(-+C*|{A' );
define( 'LOGGED_IN_SALT',   'P|HJn[N-&@KgqvRnP>uN9[(Z:J4Yf|2]VBV2ml5 GzW>Cu3%?EE|Sy%Oaopoez!o' );
define( 'NONCE_SALT',       'pRmY$bSYR;eCM{r81._~LWP.Bu&CGpC3M=A@yf&i%74UvzzmY2[D0WBXNP,doxop' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
